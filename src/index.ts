import 'reflect-metadata'
import {Client, DIService} from "discordx";
import {dirname, importx} from '@discordx/importer'
import * as dotenv from 'dotenv'
import {CommandInteraction, Intents} from "discord.js";
import {container} from "tsyringe"
import UserError from "./error/UserError.js";
import {setClient} from "./static/ClientHolder.js";

let client: Client;
await init()

async function init() {
    initEnvironment()
    setupDI()
    client = createClient()
    registerClientInjectable()
    initCommands()
    await startBot()
    setClient(client)
}

function isProduction() {
    return process.env.NODE_ENV && process.env.NODE_ENV === 'production'
}

function initEnvironment() {
    if (!isProduction()) {
        dotenv.config({path: `${process.cwd()}/.env.dev`})
    }
}

function setupDI() {
    DIService.container = container
}

function createClient(): Client {
    if (isProduction()) {
        return getProductionClient()
    } else {
        return getDevelopmentClient()
    }
}

function getProductionClient(): Client {
    return new Client({
        intents: [
            Intents.FLAGS.GUILDS,
            Intents.FLAGS.GUILD_MESSAGES,
            Intents.FLAGS.GUILD_MEMBERS,
        ],
        silent: false,
    })
}

function getDevelopmentClient(): Client {
    return new Client({
        intents: [
            Intents.FLAGS.GUILDS,
            Intents.FLAGS.GUILD_MESSAGES,
            Intents.FLAGS.GUILD_MEMBERS,
        ],
        silent: false,
    })
}

function registerClientInjectable() {
    container.register<Client>(Client, {useValue: client})
}

function initCommands() {
    initCommandStructures()
    initCommandInteraction()
}

function initCommandStructures() {
    client.once('ready', async () => {
        // await client.clearApplicationCommands()
        // await client.initApplicationCommands()
    })
}

function initCommandInteraction() {
    client.on('interactionCreate', async (interaction) => {
        const commandInteraction = interaction as CommandInteraction

        try {
            await client.executeInteraction(interaction)
        } catch (e) {
            if (e instanceof UserError) {
                if (commandInteraction.replied) {
                    await commandInteraction.editReply({
                        content: "❌ " +e.message,
                    })
                } else {
                    await commandInteraction.reply({
                        content: "❌ " +e.message,
                        ephemeral: e.ephemeral
                    })
                }

                console.log(e)
            } else {
                console.log(e)
            }
        }
    })
}

async function startBot() {
    await importClasses()

    if (process.env.DISCORD_TOKEN) {
        await client.login(process.env.DISCORD_TOKEN)
    } else {
        throw new Error('Token is not defined. Please take a look into the' +
            '.env.dev or .env.prod file depending on your environment')
    }
}

async function importClasses() {
    await importx(
        dirname(import.meta.url) + "/{events,commands,api}/**/*.js",
    )

    await importx(
        dirname(import.meta.url) + "/App.js"
    )
}
