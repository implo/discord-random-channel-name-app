enum WordGenus {
    FEMALE = <any> "FEMALE",
    MALE = <any> "MALE",
    NEUTER = <any> "NEUTER"
}

export default WordGenus
