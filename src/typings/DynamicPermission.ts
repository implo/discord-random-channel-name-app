import {ApplicationCommandPermissions, Guild} from "discord.js";
import {ApplicationCommandMixin, SimpleCommandMessage} from "discordx";

type DynamicPermission = (guild: Guild, command: ApplicationCommandMixin | SimpleCommandMessage) => ApplicationCommandPermissions | ApplicationCommandPermissions[]

export default DynamicPermission
