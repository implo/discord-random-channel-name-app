import {Channel, Guild} from "discord.js";

export default interface JobDisplayData {
    guild: Guild,
    channel: Channel,
    prefix: string,
    intervalInMin: number
}
