import {Message, User} from "discord.js";

export default interface ReplyPayload<T> {
    id: string
    content: T
    originalMessage: Message,
    originalInteractionAuthor: User
}
