export default interface JobDto {
    guildId: string
    channelId: string,
    interval: number,
    prefix: string
}
