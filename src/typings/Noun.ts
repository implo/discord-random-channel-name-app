import WordGenus from "./WordGenus.js";

export default interface Noun {
    genus: WordGenus
    text: string
}
