import {Guild} from "discord.js";

export default interface GuildScopedPageListEntry {
    text: string
    guild?: Guild
}
