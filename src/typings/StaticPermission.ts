import {ApplicationCommandPermissions} from "discord.js";

export default interface StaticPermission {
    guildId: string
    permission: ApplicationCommandPermissions
}
