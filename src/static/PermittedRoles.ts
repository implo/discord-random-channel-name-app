import {ApplicationCommandPermissions} from "discord.js";
import DynamicPermission from "../typings/DynamicPermission.js";
import StaticPermission from "../typings/StaticPermission.js";

export default class PermittedRoles {

    static adminsRoles: StaticPermission[] = []
    static moderatorRoles: StaticPermission[] = []

    static ownerRole(ownerId: string): ApplicationCommandPermissions {
        return {id: ownerId, type: 'USER', permission: true}
    }

    static owner: DynamicPermission = (guild, _) => {
        return [PermittedRoles.ownerRole(guild.ownerId)]
    }

    static admins: DynamicPermission = (guild, _) => {
        return PermittedRoles.adminsRoles
            .map(permission => permission.permission)
            .concat(PermittedRoles.ownerRole(guild.ownerId))
    }

    static moderator: DynamicPermission = (guild, _) => {
        return PermittedRoles.moderatorRoles.map(permission => permission.permission)
            .concat(PermittedRoles.adminsRoles.map(permission => permission.permission))
            .concat(PermittedRoles.ownerRole(guild.ownerId))
    }
}
