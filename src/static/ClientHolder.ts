import {Client} from "discordx";

let client: Client

export function getClient() {
    return client
}

export function setClient(newClient: Client) {
    client = newClient
}
