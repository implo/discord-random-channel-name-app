export default class {
    public static async tryFor(time: number, promise: Promise<any>) {
        const timeout = new Promise((resolve, reject) => {
            setTimeout(() => {
                reject()
            }, time)
        })

        await Promise.race([promise, timeout])
    }
}
