import Dao from "./Dao.js";

export default interface ListablePageDao<T, Listable> extends Dao<T> {
    getListableData(): Promise<Listable[]>
}
