import {injectable} from "tsyringe";
import DataController from "../control/DataController.js";
import Noun from "../typings/Noun.js";
import CacheController from "../control/CacheController.js";
import UserError from "../error/UserError.js";
import ListablePageDao from "./ListablePageDao";
import DataBase from "./DataBase.js";

@injectable()
export default class NounDao implements ListablePageDao<Noun, string>{

    private referencePath = 'nouns'

    constructor(
        private dataController: DataController,
        private database: DataBase,
        private cache: CacheController
    ) {
    }

    async removeById(text: string): Promise<void> {
        try {
            const nouns = await this.getAll()
            this.dataController
                .removeItemFromJsonByCriteria(nouns, noun => noun.text === text)

            const nounsReference = await this.database.getCollection(this.referencePath)
            const nounsToDelete = await nounsReference.where('text', '==', text).get()
            nounsToDelete.forEach(noun => noun.ref.delete())
        } catch (e) {
            throw new UserError('noun does not exist')
        }
    }

    async getListableData(): Promise<string[]> {
        return (await this.getAll())
            .map(noun => noun.text)
    }

    async getAll(): Promise<Noun[]> {
        if (this.cache.nounCache.store) {
            return this.cache.nounCache.store
        } else {
            const nouns = (await this.database.findAll(this.referencePath)) as Noun[]
            this.cache.nounCache.store = nouns
            return nouns
        }
    }

    async save(noun: Noun) {
        const nouns = await this.getAll()
        nouns.push(noun)
        await this.database.insertOne(this.referencePath, noun)
    }

    async exists(noun: Noun): Promise<boolean> {
        const nouns = await this.getAll()
        return nouns.filter(nounEntry => noun.text === nounEntry.text).length !== 0
    }

}
