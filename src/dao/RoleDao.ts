import Dao from "./Dao.js";
import DataController from "../control/DataController.js";
import UserError from "../error/UserError.js";
import StaticPermission from "../typings/StaticPermission.js";
import FileDao from "./FileDao.js";

export default abstract class RoleDao implements Dao<StaticPermission>, FileDao {

    abstract get filepath(): string
    abstract get cachedRoles(): StaticPermission[]

    protected constructor(
        private dataController: DataController
    ) {}

    async getAll(): Promise<StaticPermission[]> {
        const roles = await this.dataController.readFile(this.filepath)
        return JSON.parse(roles)
    }

    async removeById(roleId: string): Promise<void> {
        try {
            console.log(this.cachedRoles)
            this.dataController.removeItemFromJsonByCriteria(
                this.cachedRoles,
                role => role.permission.id === roleId
            )

            await this.dataController.writeJson(this.filepath, this.cachedRoles)
        } catch (e) {
            throw new UserError('role is not mapped to any permission')
        }
    }

    async save(entity: StaticPermission): Promise<void> {
        this.cachedRoles.push(entity)
        await this.dataController.writeJson(this.filepath, this.cachedRoles)
    }

    async initFileIfMissing() {
        const fileExists = await this.dataController.fileOrDirectoryExists(this.filepath)
        if (!fileExists) {
            await this.dataController.writeJson(this.filepath, [])
        }
    }

}
