import {injectable} from "tsyringe";
import {initializeApp, App, cert} from "firebase-admin/app";
import {getFirestore} from "firebase-admin/firestore";
import * as fs from "fs";

@injectable()
export default class DataBase {

    private app!: App

    private get firestore() {
        return getFirestore(this.app)
    }

    async init() {
        const fireStoreConfigString = await fs.promises.readFile(`${process.cwd()}/firestore_auth.json`, 'utf-8')
        const fireStoreConfig = JSON.parse(fireStoreConfigString)
        this.app = initializeApp({
            credential: cert(fireStoreConfig)
        })
    }

    async findAll(id: string): Promise<any[]> {
        const collectionRef = this.firestore
            .collection(id)
        const snapshot = await collectionRef.get()
        const collection: any[] = []
        snapshot.forEach(document => collection.push(document.data() as any))
        return collection
    }

    async insertMany(id: string, ...documents: any[]) {
        const batch = this.firestore.batch()

        documents.forEach(document => {
            const documentReference = this.firestore
                .collection(id)
                .doc()

            batch.set(documentReference, document)
        })

        await batch.commit()
    }

    async insertOne(id: string, document: any) {
        await this.firestore.collection(id).add(document)
    }

    async getCollection(id: string) {
        return this.firestore.collection(id)
    }

}
