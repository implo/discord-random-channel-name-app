import GuildScopedPageListEntry from "../typings/GuildScopedPageListEntry";
import ListablePageDao from "./ListablePageDao";

export default interface GuildScopedListablePageDao<T> extends ListablePageDao<T, GuildScopedPageListEntry> {}
