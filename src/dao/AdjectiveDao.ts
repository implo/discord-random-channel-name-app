import {injectable} from "tsyringe";
import DataController from "../control/DataController.js";
import CacheController from "../control/CacheController.js";
import Adjective from "../typings/Adjective.js";
import UserError from "../error/UserError.js";
import ListablePageDao from "./ListablePageDao";
import DataBase from "./DataBase.js";

@injectable()
export default class AdjectiveDao implements ListablePageDao<Adjective, string>{

    private referencePath: string = 'adjectives'

    constructor(
        private dataController: DataController,
        private database: DataBase,
        private cache: CacheController
    ) {
    }

    async getAll() {
        const cache = this.cache.adjectiveCache.store
        if (cache) {
            return cache
        } else {
            const adjectives = await (this.database.findAll(this.referencePath)) as Adjective[]
            this.cache.adjectiveCache.store = adjectives
            return adjectives
        }
    }

    async save(adjective: Adjective) {
        const adjectives = await this.getAll()
        adjectives.push(adjective)
        await this.database.insertOne(this.referencePath, adjective)
    }

    async getListableData(): Promise<string[]> {
        return (await this.getAll())
            .map(adjective => adjective.text)
    }

    async removeById(text: string): Promise<void> {
        const adjectives = await this.getAll()

        const adjectivesToRemove = adjectives.filter(adjective => adjective.text === text)
        console.log(adjectives)
        if (adjectivesToRemove.length !== 0) {
            adjectivesToRemove.forEach(adjectiveToRemove => {
                const index = adjectives.indexOf(adjectiveToRemove)
                adjectives.splice(index, 1)
            })

            const adjectiveReference = await this.database.getCollection(this.referencePath)
            const adjectivesToDelete = await adjectiveReference.where('text', '==', text).get()
            adjectivesToDelete.forEach(doc => doc.ref.delete())
        } else {
            throw new UserError('adjective does not exist')
        }
    }
}
