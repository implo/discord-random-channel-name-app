import JobDto from "../typings/JobDto.js";
import {singleton} from "tsyringe";
import CacheController from "../control/CacheController.js";
import DataController from "../control/DataController.js";
import {getClient} from "../static/ClientHolder.js";
import JobDisplayData from "../typings/JobDisplayData";
import {Channel} from "discord.js";
import GuildScopedListablePageDao from "./GuildScopedListablePageDao.js";
import GuildScopedPageListEntry from "../typings/GuildScopedPageListEntry.js";
import RenameJob from "../control/RenameJob";
import FileDao from "./FileDao.js";

@singleton()
export default class JobDao implements GuildScopedListablePageDao<JobDto>, FileDao {

    private readonly filename: string = 'jobs.json'
    private schedules: RenameJob[]
    private test: string = ''

    constructor(
        private cache: CacheController,
        private dataController: DataController
    ) {
        this.schedules = []
    }

    async getAll(): Promise<JobDto[]> {
        if (this.cache.jobCache.store) {
            return this.cache.jobCache.store
        } else {
            return JSON.parse(await this.dataController.readFile(this.filename))
        }
    }

    async removeById(channelId: string): Promise<void> {
        const jobs = await this.getAll()

        this.dataController.removeItemFromJsonByCriteria(jobs, job => {
            return job.channelId === channelId
        })

        await this.dataController.writeJson(this.filename, jobs)
    }

    async save(entity: JobDto): Promise<void> {
        const jobs = await this.getAll()
        jobs.push(entity)
        await this.dataController.writeJson(this.filename, jobs)
    }

    async scheduleJob(job: RenameJob) {
        await job.start()
        this.schedules.push(job)
        this.test = 'tested'
    }

    async stopJob(channelId: string)  {
        this.schedules
            .filter(job => job.channelId === channelId)
            .forEach(job => job.stop())

        this.schedules = this.schedules
            .filter(job => job.channelId !== channelId)
    }

    async getListableData(): Promise<GuildScopedPageListEntry[]> {
        const jobs = await this.getAll()

        const displayData = await Promise.all(jobs.map(async job => {
            const client = getClient()
            const guild = await client.guilds.fetch(job.guildId)
            const channel = await guild.channels.fetch(job.channelId) as Channel

            const displayData: JobDisplayData = {
                guild,
                channel,
                intervalInMin: job.interval,
                prefix: job.prefix
            }

            return displayData
        }))

        const sortedDisplayData = displayData.sort((display1, display2) => {
            if (display1.channel.isText() && display2.channel.isVoice()) {
                return 1;
            } else {
                return -1
            }
        })

        return sortedDisplayData.map(display => {
            return {
                text: `${display.channel} - ${display.intervalInMin} min - ${display.prefix ? display.prefix : 'none set'}`,
                guild: display.guild
            }
        })
    }

    async initFileIfMissing() {
        const fileExists = await this.dataController.fileOrDirectoryExists(this.filename)
        if (!fileExists) {
            await this.dataController.writeJson(this.filename, [])
        }
    }

}
