export default interface FileDao {
    initFileIfMissing(): Promise<void>
}
