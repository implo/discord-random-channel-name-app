import RoleDao from "./RoleDao.js";
import PermittedRoles from "../static/PermittedRoles.js";
import {injectable} from "tsyringe";
import DataController from "../control/DataController.js";
import StaticPermission from "../typings/StaticPermission";

@injectable()
export default class AdminRoleDao extends RoleDao {

    constructor(dataController: DataController) {
        super(dataController);
    }

    get cachedRoles(): StaticPermission[] {
        return PermittedRoles.adminsRoles;
    }

    get filepath(): string {
        return "adminRoles.json";
    }

}
