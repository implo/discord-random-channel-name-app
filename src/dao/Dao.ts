export default interface Dao<T> {
    getAll(): Promise<T[]>
    save(entity: T): Promise<void>
    removeById(adjective: string): Promise<void>
}
