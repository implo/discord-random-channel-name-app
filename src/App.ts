import {Client, Discord, On, Once} from "discordx";
import Database from "./dao/DataBase.js";
import DataController from "./control/DataController.js";
import CacheController from "./control/CacheController.js";
import RandomNameGenerator from "./control/RandomNameGenerator.js";
import ModeratorRoleDao from "./dao/ModeratorRoleDao.js";
import AdminRoleDao from "./dao/AdminRoleDao.js";
import JobDao from "./dao/JobDao.js";
import RenameJob from "./control/RenameJob.js";
import PermittedRoles from "./static/PermittedRoles.js";
import {injectable} from "tsyringe";

@Discord()
@injectable()
export default class App {

    constructor(
        private database: Database,
        private fileController: DataController,
        private cacheController: CacheController,
        private randomGenerator: RandomNameGenerator,
        private moderatorRoleDao: ModeratorRoleDao,
        private adminRoleDao: AdminRoleDao,
        private jobDao: JobDao,
        private dataController: DataController,
        private client: Client
    ) { }

    @Once('ready')
    async init(): Promise<void> {
        await this.initializeDataStores()

        await Promise.all([
            this.initializeJobs(),
            this.initializeCommands()
        ])
    }

    @On('guildCreate')
    async initGuild(): Promise<void> {
        await this.initializeCommands()
    }

    private async initializeDataStores() {
        return await Promise.all([
            this.initializeFileStore(),
            this.initializeFireStore(),
        ])
    }

    private async initializeFileStore() {
        await this.dataController.createDirectoryIfMissing()
        await Promise.all([
            this.jobDao.initFileIfMissing(),
            this.moderatorRoleDao.initFileIfMissing(),
            this.adminRoleDao.initFileIfMissing()
        ])
    }

    private async initializeFireStore() {
        await this.database.init()
    }


    private async initializeJobs() {
        const jobs = await this.jobDao.getAll()

        for (const job of jobs) {
            const scheduleJob = await RenameJob.fromDataObject(
                job,
                this.client,
                this.randomGenerator
            )

            await this.jobDao.scheduleJob(scheduleJob)
        }
    }

    private async initializeCommands() {
        await this.fetchPermissions()
        this.registerCommandGuilds()
        await this.updateCommands()
        await this.updatePermissions()
    }

    private async fetchPermissions() {
        [PermittedRoles.moderatorRoles, PermittedRoles.adminsRoles] = await Promise.all([
            this.moderatorRoleDao.getAll(),
            this.adminRoleDao.getAll()
        ])
    }

    get guilds() {
        return this.client.guilds.cache
    }

    get guildIds() {
        return this.guilds.map(guild => guild.id)
    }

    private registerCommandGuilds() {
        this.client.botGuilds.push(...this.guildIds)
    }

    private async updateCommands() {
        await this.client.clearApplicationCommands()
        await this.client.initApplicationCommands()
    }

    private async updatePermissions() {
        await this.client.initApplicationPermissions()
    }
}
