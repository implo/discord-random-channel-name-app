import {Discord, Permission, Slash, SlashGroup, SlashOption} from "discordx";
import PermittedRoles from "../static/PermittedRoles.js";
import {CommandInteraction, Role} from "discord.js";
import AdminRoleDao from "../dao/AdminRoleDao.js";
import ModeratorRoleDao from "../dao/ModeratorRoleDao.js";
import {getClient} from "../static/ClientHolder.js";
import {injectable} from "tsyringe";

@SlashGroup('revoke')
@Permission(PermittedRoles.admins)
@Permission(false)
@Discord()
@injectable()
export default class RevokeCommand {

    constructor(
        private adminRoleDao: AdminRoleDao,
        private moderatorRoleDao: ModeratorRoleDao
    ) { }

    @Slash('admin')
    async revokeAdmin(

        @SlashOption('from')
        role: Role,

        interaction: CommandInteraction
    ) {
        await interaction.reply('🔑 removing permission')
        await this.adminRoleDao.removeById(role.id)
        await interaction.editReply('🔑 updating permissions')
        await getClient().initApplicationPermissions()
        await interaction.editReply(`👍 revoked admin from ${role}`)
    }

    @Slash('moderator')
    async revokeModerator(

        @SlashOption('from')
            role: Role,

        interaction: CommandInteraction
    ) {
        await interaction.reply('🔑 removing permission')
        await this.moderatorRoleDao.removeById(role.id)
        await interaction.editReply('🔑 updating permissions')
        await getClient().initApplicationPermissions()
        await interaction.editReply(`👍 revoked moderator from ${role}`)
    }

}
