import {injectable} from "tsyringe";
import {Client, Discord, Permission, Slash, SlashGroup, SlashOption} from "discordx";
import PermittedRoles from "../static/PermittedRoles.js";
import {CommandInteraction, Role} from "discord.js";
import ModeratorRoleDao from "../dao/ModeratorRoleDao.js";
import AdminRoleDao from "../dao/AdminRoleDao.js";
import StaticPermission from "../typings/StaticPermission";

@SlashGroup('permit')
@Permission(false)
@Permission(PermittedRoles.admins)
@Discord()
@injectable()
export default class PermitCommand {

    constructor(
        private moderatorRoleDao: ModeratorRoleDao,
        private adminRoleDao: AdminRoleDao,
        private client: Client
    ) {
    }

    @Slash('admin')
    async permitAdmin(
        @SlashOption('to')
            role: Role,

        interaction: CommandInteraction
    ) {
        await interaction.reply('🔑 adding permissions')
        await this.adminRoleDao.save(PermitCommand.createRolePermission(role, interaction.guildId!))
        await interaction.editReply('🔑 updating permissions')
        await this.client.initApplicationPermissions()
        await interaction.editReply(`👍 permitted ${role} admin permissions`)
    }

    @Slash('moderator')
    async permitModarator(
        @SlashOption('to')
            role: Role,

        interaction: CommandInteraction
    ) {
        await interaction.reply('🔑 adding permissions')
        await this.moderatorRoleDao.save(PermitCommand.createRolePermission(role, interaction.guildId!))
        await interaction.editReply('🔑 updating permissions')
        await this.client.initApplicationPermissions()
        await interaction.editReply(`👍 permitted ${role} moderator permissions`)
    }

    private static createRolePermission(role: Role, guildId: string): StaticPermission {
        return {
            guildId,
            permission: {
                id: role.id,
                type: "ROLE",
                permission: true
            }
        }
    }
}
