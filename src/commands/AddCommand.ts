import {ButtonComponent, Discord, Permission, Slash, SlashChoice, SlashGroup, SlashOption} from "discordx";
import {ButtonInteraction, Channel, CommandInteraction} from "discord.js";
import CacheController from "../control/CacheController.js";
import {injectable} from "tsyringe";
import AddAdjectiveExecution from "./executions/AddAdjectiveExecution.js";
import ReplyPayload from "../typings/ReplyPayload";
import AdjectiveDao from "../dao/AdjectiveDao.js";
import UserError from "../error/UserError.js";
import WordGenus from "../typings/WordGenus.js";
import NounDao from "../dao/NounDao.js";
import AddNounExecution from "./executions/AddNounExecution.js";
import PermittedRoles from "../static/PermittedRoles.js";
import AddJobExecution from "./executions/AddJobExecution.js";
import JobDao from "../dao/JobDao.js";
import RandomNameGenerator from "../control/RandomNameGenerator.js";

@SlashGroup('add')
@Discord()
@Permission(false)
@Permission(PermittedRoles.moderator)
@injectable()
export default class AddCommand {

    payloads: Map<string, ReplyPayload<any>> = new Map<string, ReplyPayload<any>>()

    constructor(
        private adjectiveDao: AdjectiveDao,
        private nounDao: NounDao,
        private jobDao: JobDao,
        private generator: RandomNameGenerator,
        private cache: CacheController
    ) {}

    @Slash('adjective')
    async addAdjective(
        @SlashOption('word', {description: 'please enter a adjective without genus'})
            word: string,

        interaction: CommandInteraction
    ) {
        const execution = new AddAdjectiveExecution(interaction, this.adjectiveDao, word, this.cache)
        await execution.execute()

        if (execution.payload) {
            this.payloads.set(execution.payload.id, execution.payload)
        }
    }

    @ButtonComponent("add-word-yes")
    async saysYes(interaction: ButtonInteraction) {
        const payload = this.getStringPayload(interaction.message.id)
        await this.adjectiveDao.save({ text: payload.content })
        await this.setDone(interaction)
        await interaction.followUp(`👍 added ${payload.content}`);
    }

    @ButtonComponent("add-word-no")
    async saysNo(interaction: ButtonInteraction) {
        const payload = this.getStringPayload(interaction.message.id)
        await this.setDone(interaction)
        await interaction.followUp(`👍 not adding ${payload.content}`)
    }

    private async setDone(interaction: ButtonInteraction) {
        const payload = this.getStringPayload(interaction.message.id)
        const components = payload.originalMessage.components

        components.forEach(row => row.components.forEach(component => {
            component.setDisabled(true)
        }))

        await interaction.update({components})
    }

    @Slash('noun')
    async addNoun(
        @SlashOption('word')
            noun: string,

        @SlashChoice(WordGenus)
        @SlashOption('genus')
            genus: string,

        interaction: CommandInteraction,
    ) {
        const execution = new AddNounExecution(interaction, this.cache, this.nounDao, noun, genus)
        await execution.execute()
    }

    getStringPayload(id: string) {
        const payload = this.payloads.get(id)
        if (payload && typeof payload.content === 'string') {
            return payload
        } else {
            throw new UserError(
                'please try sending the message again',
                false,
                'the payload is invalid. Wrong type'
            )
        }
    }

    @Slash('job')
    async schedule(

        @SlashOption('channel')
            channel: Channel,

        @SlashOption('interval', {
            required: false,
            description: 'interval in minutes. minimum is 30'
        })
            intervalInMin: number,

        @SlashOption('prefix', {required: false})
            prefix: string,

        interaction: CommandInteraction
    ) {
        const execution = new AddJobExecution(
            interaction,
            this.generator,
            this.jobDao,
            channel,
            intervalInMin,
            prefix
        )

        await execution.execute()
    }

}
