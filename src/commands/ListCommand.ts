import {injectable} from "tsyringe";
import {ButtonComponent, Discord, Slash, SlashGroup, SlashOption} from "discordx";
import {ButtonInteraction, CommandInteraction} from "discord.js";
import AdjectiveDao from "../dao/AdjectiveDao.js";
import ReplyPayload from "../typings/ReplyPayload.js";
import PageChangeExecution from "./executions/PageChangeExecution.js";
import NounDao from "../dao/NounDao.js";
import ListExecution from "./executions/ListExecution.js";
import JobDao from "../dao/JobDao.js";
import GuildScopedListExecution from "./executions/GuildScopedListExecution.js";

@SlashGroup('list')
@Discord()
@injectable()
export default class ListCommand {

    private readonly payloads: Map<string, ReplyPayload<any>>
        = new Map<string, ReplyPayload<any>>()

    constructor(private adjectiveDao: AdjectiveDao, private nounDao: NounDao, private jobDao: JobDao) {}

    @Slash('adjectives')
    async listAdjectives(

        @SlashOption('search', {description: 'search term for the list', required: false})
            search: string,

        interaction: CommandInteraction

    ) {
        const execution = new ListExecution(
            interaction,
            this.adjectiveDao,
            'adjective-list',
            'Adjectives',
            search
        )

        await execution.execute()

        this.payloads.set(
            execution.payload.id,
            execution.payload
        )
    }

    @ButtonComponent('next-adjective-list')
    async nextAdjectiveList(interaction: ButtonInteraction) {
        const execution = new PageChangeExecution(interaction, this.payloads)
        await execution.nextPage()
    }

    @ButtonComponent('prev-adjective-list')
    async prevAdjectiveList(interaction: ButtonInteraction) {
        const execution = new PageChangeExecution(interaction, this.payloads)
        await execution.prevPage()
    }

    @Slash('nouns')
    async listNouns(
        @SlashOption('search', {description: 'search term for the list', required: false})
            search: string,

        interaction: CommandInteraction
    ) {
        const execution = new ListExecution(
            interaction,
            this.nounDao,
            'nouns-list',
            'Nouns',
            search
        );

        await execution.execute()
        this.payloads.set(execution.payload.id, execution.payload)
    }

    @ButtonComponent('next-nouns-list')
    async nextNounList(interaction: ButtonInteraction) {
        const execution = new PageChangeExecution(interaction, this.payloads)
        await execution.nextPage()
    }

    @ButtonComponent('prev-nouns-list')
    async prevNounList(interaction: ButtonInteraction) {
        const execution = new PageChangeExecution(interaction, this.payloads)
        await execution.prevPage()
    }

    @Slash('jobs')
    async listJobs(interaction: CommandInteraction) {
        const execution = new GuildScopedListExecution(
            interaction,
            this.jobDao,
            'job-list',
            'Jobs'
        )

        await execution.execute()
    }

}
