import CommandExecution from "./CommandExecution.js";
import AdjectiveDao from "../../dao/AdjectiveDao.js";
import {CommandInteraction, Message} from "discord.js";
import PageWidget from "../../control/PageWidget.js";
import Adjective from "../../typings/Adjective.js";
import ReplyPayload from "../../typings/ReplyPayload.js";

export default class ListAdjectiveExecution implements CommandExecution {

    private adjectives?: Adjective[]
    private widget?: PageWidget
    private payload?: ReplyPayload<PageWidget>

    constructor(
        private interaction: CommandInteraction,
        private adjectiveDao: AdjectiveDao
    ) {}

    get resolvedPayload() {
        if (this.payload) {
            return this.payload
        } else {
            throw 'payload has not been set before'
        }
    }

    private get resolvedWidget() {
        if (this.widget) {
            return this.widget
        } else {
            throw 'widget has not been initialized yet'
        }
    }

    private get resolvedAdjectives() {
        if (this.adjectives) {
            return this.adjectives
        } else {
            throw 'adjectives has not been initialized yet'
        }
    }

    async execute(): Promise<void> {
        await this.loadAdjectives()
        this.initWidget()
        await this.replyWithWidget()


        return Promise.resolve(undefined);
    }

    private async loadAdjectives() {
        this.adjectives = await this.adjectiveDao.getAll()
    }

    private initWidget() {
        this.widget = new PageWidget(
            'adjective-list',
            'Adjectives',
            "BLUE",
            this.adjectivesAsStrings
        )
    }

    private get adjectivesAsStrings() {
        return this.resolvedAdjectives
            .map(adjective => adjective.text)

    }

    private async replyWithWidget() {
        const replyMessage = await this.interaction.reply(
            this.resolvedWidget.message
        )

        this.producePayload(replyMessage as Message)
    }

    private producePayload(message : Message) {
        this.payload = {
            id: message.id,
            content: this.resolvedWidget,
            originalInteractionAuthor: this.interaction.user,
            originalMessage: message
        }
    }
}
