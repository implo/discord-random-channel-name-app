import CommandExecution from "./CommandExecution.js";
import {Channel, CommandInteraction, Guild, GuildChannel} from "discord.js";
import JobDao from "../../dao/JobDao.js";
import RenameJob from "../../control/RenameJob.js";
import RandomGenerator from "../../control/RandomGenerator.js";
import UserError from "../../error/UserError.js";

export default class AddJobExecution implements CommandExecution {

    private readonly guildChannel: GuildChannel
    private readonly guild: Guild
    private readonly minimizedInterval?: number
    private job!: RenameJob

    constructor(
        private interaction: CommandInteraction,
        private generator: RandomGenerator<string>,
        private jobDao: JobDao,
        private channel: Channel,
        private interval?: number,
        private prefix?: string
    ) {
        if (this.channel.isVoice() || this.channel.isText()) {
            this.guildChannel = channel as GuildChannel
            this.guild = this.guildChannel.guild
            this.minimizedInterval = interval ? Math.min(interval, 30) : interval
        } else {
            throw AddJobExecution.invalidChannel
        }
    }


    async execute() {
        if (!(await this.channelHasJob())) {
            await this.addJob()
            await this.reply()
        } else {
            AddJobExecution.throwAlreadyExists()
        }
    }

    private async channelHasJob() {
        const jobs = await this.jobDao.getAll()
        const jobsWithOverlappingChannelId = jobs.filter(job => job.channelId === this.channel.id)
        return jobsWithOverlappingChannelId.length > 0
    }

    private async addJob() {
        await Promise.all([
            this.scheduleJob(),
            this.persistJob()
        ])
    }

    private async scheduleJob() {
        this.job = new RenameJob(
            this.guild,
            this.guildChannel,
            this.generator,
            this.prefix,
            this.minimizedInterval
        )

        await this.jobDao.scheduleJob(this.job)
    }

    private async persistJob() {
        await this.jobDao.save(this.job.toDataObject())
    }

    private async reply() {
        await this.interaction.reply('⏰ Job scheduled')
    }

    private static get invalidChannel() {
        return new UserError('invalid channel. Channel must' +
            ' be a text or a voice channel')
    }

    private static throwAlreadyExists() {
        throw new UserError('Job allready scheduled ' +
            'on given channel. please ' +
            'remove job and create again ' +
            'to change settings')
    }
}
