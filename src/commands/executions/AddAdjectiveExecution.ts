import CommandExecution from "./CommandExecution.js";
import {CommandInteraction, Message, MessageActionRow, MessageButton} from "discord.js";
import CacheController from "../../control/CacheController.js";
import Adjective from "../../typings/Adjective.js";
import UserError from "../../error/UserError.js";
import ReplyPayload from "../../typings/ReplyPayload";
import AdjectiveDao from "../../dao/AdjectiveDao.js";

export default class AddAdjectiveExecution implements CommandExecution {

    private adjectives: Adjective[] = []
    public payload?: ReplyPayload<string>

    constructor(
        private interaction: CommandInteraction,
        private adjectiveDao: AdjectiveDao,
        private word: string,
        cache: CacheController,
    ) {
        if (cache.adjectiveCache.store) {
            this.adjectives = cache.adjectiveCache.store
        } else {
            throw Error('Adjective Cache is invalid')
        }
    }

    async execute(): Promise<void> {
        if (this.wordExists(this.word)) {
            await this.saveIfValid()
        } else {
            throw new UserError('word already exists')
        }
    }

    private wordExists(word: string) {
        return this.adjectives
            .filter(wordEntry => wordEntry.text === word)
            .length === 0
    }

    private async saveIfValid() {
        if (this.wordIsValid()) {
            await this.save()
        } else {
            await this.askIfUserIsSure()
        }
    }

    private wordIsValid() {
        return !(this.word.endsWith('er') || this.word.endsWith('es') || this.word.endsWith('e'));
    }

    private async save() {
        await this.interaction.reply('adding word ...')
        await this.adjectiveDao.save({text: this.word})
        await this.interaction.editReply(`👍 added ${this.word}`)
    }

    private async askIfUserIsSure() {
        const yesButton = new MessageButton()
            .setLabel('Yes')
            .setStyle("PRIMARY")
            .setEmoji("✅")
            .setCustomId('add-word-yes')

        const noButton = new MessageButton()
            .setLabel('No')
            .setStyle("PRIMARY")
            .setEmoji("❌")
            .setCustomId('add-word-no')

        const actionRow = new MessageActionRow()
            .addComponents(yesButton, noButton)

        const message = await this.interaction.reply({
            content: 'Are you sure this word is without genus',
            components: [actionRow],
            fetchReply: true,
            ephemeral: true
        })

        this.payload = {
            id: message.id,
            content: this.word,
            originalMessage: message as Message,
            originalInteractionAuthor: this.interaction.user
        }
    }
}
