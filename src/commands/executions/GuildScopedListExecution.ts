import {CommandInteraction} from "discord.js";
import GuildScopedListablePageDao from "../../dao/GuildScopedListablePageDao.js";
import AbstractListExecution from "./AbstractListExecution.js";

export default class GuildScopedListExecution extends AbstractListExecution {

    constructor(
        interaction: CommandInteraction,
        private dao: GuildScopedListablePageDao<any>,
        id: string,
        title: string,
    ) {
        super(interaction, id, title);
    }

    async loadData(): Promise<void> {
        const entries = await this.dao.getListableData()

        this.data = entries
            .filter(entry => entry.guild === this.interaction.guild)
            .map(entry => entry.text)
    }


}
