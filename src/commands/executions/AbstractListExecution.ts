import PageWidget from "../../control/PageWidget.js";
import ReplyPayload from "../../typings/ReplyPayload.js";
import {ColorResolvable, CommandInteraction, Message} from "discord.js";

export default abstract class AbstractListExecution {

    protected data!: string[]
    protected widget!: PageWidget
    public payload!: ReplyPayload<PageWidget>

    protected constructor(
        protected interaction: CommandInteraction,
        protected id: string,
        protected title: string = 'List',
        protected color: ColorResolvable = 'BLUE',
        protected ephemeral: boolean = true
    ) {
    }

    async execute(): Promise<void> {
        await this.loadData()
        this.initWidget()
        await this.replyWithWidget()
    }

    abstract loadData(): Promise<void>

    private initWidget() {
        this.widget = new PageWidget(
            this.id,
            this.title,
            this.color,
            this.data,
            this.ephemeral
        )
    }

    private async replyWithWidget() {
        const replyMessage = await this.interaction.reply(
            this.widget.message
        )

        this.producePayload(replyMessage as Message)
    }

    private producePayload(message : Message) {
        this.payload = {
            id: message.id,
            content: this.widget,
            originalInteractionAuthor: this.interaction.user,
            originalMessage: message
        }
    }
}
