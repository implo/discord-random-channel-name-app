import CommandExecution from "./CommandExecution.js";
import {CommandInteraction} from "discord.js";
import NounDao from "../../dao/NounDao.js";
import WordGenus from "../../typings/WordGenus.js";
import Noun from "../../typings/Noun.js";
import CacheController from "../../control/CacheController.js";
import UserError from "../../error/UserError.js";

export default class AddNounExecution implements CommandExecution{

    private noun: Noun

    constructor(
        private interactions: CommandInteraction,
        private cache: CacheController,
        private nounDao: NounDao,
        nounString: string,
        genusString: string
    ) {
        const genus: WordGenus = (<any>WordGenus)[genusString]
        this.noun = {text: nounString, genus}
    }

    async execute(): Promise<void> {
        if (await this.nounNotExists()) {
            await this.saveNoun()
        } else {
            this.rejectNoun()
        }
    }

    private async nounNotExists(): Promise<boolean> {
        return ! await this.nounDao.exists(this.noun)
    }

    private async saveNoun() {
        await this.nounDao.save(this.noun)
        await this.interactions.reply(
            `added ${this.noun.text} with genus ${this.noun.genus}`
        )
    }

    private rejectNoun() {
        throw new UserError(`${this.noun.text} already exists`)
    }
}
