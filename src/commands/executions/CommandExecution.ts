export default interface CommandExecution {
    execute(): Promise<void>
}
