import {ButtonInteraction} from "discord.js";
import ReplyPayload from "../../typings/ReplyPayload";
import PageWidget from "../../control/PageWidget";

export default class PageChangeExecution {

    constructor(
        private interaction: ButtonInteraction,
        private payloads: Map<string, ReplyPayload<any>>
    ) { }

    get payload() {
        const payload = this.payloads.get(this.interaction.message.id)
        if (payload) {
            return payload
        } else {
            throw 'payload not found'
        }
    }

    get widget() {
        try {
            return this.payload.content as PageWidget
        } catch (e) {
            throw 'payload was invalid'
        }
    }

    async nextPage() {
        if (this.widget.hasNextPage()) {
            this.widget.nextPage()
        }

        await this.interaction.update(this.widget.message)
    }

    async prevPage() {
        if (this.widget.hasPrevPage()) {
            this.widget.prevPage()
        }

        await this.interaction.update(this.widget.message)
    }

}
