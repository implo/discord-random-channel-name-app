import {CommandInteraction, MessageEmbed, Snowflake} from "discord.js";
import CommandExecution from "./CommandExecution";
import StaticPermission from "../../typings/StaticPermission.js";
import PermittedRoles from "../../static/PermittedRoles.js";

export default class ViewPermissionExecution implements CommandExecution {

    private moderatorRoles: StaticPermission[] = []
    private adminRoles: StaticPermission[] = []
    private message: MessageEmbed = new MessageEmbed()

    constructor(
        private interaction: CommandInteraction
    ) {}

    async execute(): Promise<void> {
        this.loadData()
        await this.buildMessage()
        await this.reply()
    }

    private loadData() {
        this.moderatorRoles = PermittedRoles.moderatorRoles
            .filter(role => role.guildId === this.interaction.guildId)

        this.adminRoles = PermittedRoles.adminsRoles
            .filter(role => role.guildId === this.interaction.guildId)
    }

    private async buildMessage() {
        this.message
            .setTitle('Permissions')
            .setColor('BLUE')

        this.message
            .addField('Admins', await this.adminField())
            .addField('Moderator', await this.moderatorField())

    }

    private async adminField(): Promise<string> {
        return await this.getFieldOfRoles(this.adminRoles)
    }

    private async moderatorField(): Promise<string> {
        return await this.getFieldOfRoles(this.moderatorRoles)
    }

    async getFieldOfRoles(roles: StaticPermission[]) {
        let content = ''

        for (const role of roles) {
            const discordRole = await this.getDiscordRoleById(role.permission.id)
            content += `- ${discordRole} \n`
        }

        if (content === '') {
            return 'none set'
        } else {
            return content
        }
    }

    private async getDiscordRoleById(id: Snowflake) {
        return await this.interaction.guild!.roles.fetch(id)
    }

    private async reply() {
        await this.interaction.reply({embeds: [this.message]})
    }
}
