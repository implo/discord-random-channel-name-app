import ListablePageDao from "../../dao/ListablePageDao.js"
import {ColorResolvable, CommandInteraction} from "discord.js";
import AbstractListExecution from "./AbstractListExecution.js";

export default class ListExecution extends AbstractListExecution {

    constructor(
        interaction: CommandInteraction,
        private dao: ListablePageDao<any, string>,
        id: string,
        title: string = 'List',
        private search?: string,
        color: ColorResolvable = 'BLUE',
    ) {
        super(interaction, id, title, color)
    }

    async loadData(): Promise<void> {
        this.data = await this.getData()
    }

    async getData(): Promise<string[]> {
        if (this.search) {
            return (await this.dao.getListableData())
                .filter(entry => entry.includes(this.search!))
        } else {
            return await this.dao.getListableData()
        }
    }
}
