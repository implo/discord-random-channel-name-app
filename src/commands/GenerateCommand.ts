import {injectable} from "tsyringe";
import {Discord, Slash, SlashOption} from "discordx";
import {CommandInteraction} from "discord.js";
import RandomNameGenerator from "../control/RandomNameGenerator.js";

@Discord()
@injectable()
export default class GenerateCommand {

    constructor(private randomGenerator: RandomNameGenerator) {
    }

    @Slash("generate")
    public async generate(

        @SlashOption('prefix', {required: false})
            prefix: string,

        interaction: CommandInteraction,

    ) {
        if (prefix) {
            await interaction.reply(`${prefix} ${await this.randomGenerator.generate()}`);
        } else  {
            await interaction.reply(`${await this.randomGenerator.generate()}`);
        }
    }
}
