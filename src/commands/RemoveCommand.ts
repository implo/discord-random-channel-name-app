import {Discord, Permission, Slash, SlashGroup, SlashOption} from "discordx";
import {injectable} from "tsyringe";
import {Channel, CommandInteraction} from "discord.js";
import AdjectiveDao from "../dao/AdjectiveDao.js";
import NounDao from "../dao/NounDao.js";
import PermittedRoles from "../static/PermittedRoles.js";
import JobDao from "../dao/JobDao.js";

@SlashGroup('remove')
@Permission(false)
@Permission(PermittedRoles.moderator)
@Discord()
@injectable()
export default class RemoveCommand {


    constructor(
        private adjectiveDao: AdjectiveDao,
        private nounDao: NounDao,
        private jobDao: JobDao
    ) { }

    @Slash('adjective')
    async removeAdjective(
        @SlashOption('adjective', {description: 'Name of the adjective you want to remove'})
            adjective: string,

        interaction: CommandInteraction,
    ) {
        await this.adjectiveDao.removeById(adjective)
        await interaction.reply(`👍 removed ${adjective}`)
    }

    @Slash('noun')
    async removeNoun(
        @SlashOption('noun', {description: 'Name fo the noun you want to remove'})
            noun: string,

        interaction: CommandInteraction
    ) {
        await this.nounDao.removeById(noun)
        await interaction.reply(`👍 removed ${noun}`)
    }

    @Slash('job')
    async removeJob(
        @SlashOption('channel', {description: 'Channel the job is scheduled for'})
            channel: Channel,

        interaction: CommandInteraction
    ) {
        await this.jobDao.stopJob(channel.id)
        await this.jobDao.removeById(channel.id)
        await interaction.reply(`👍 removed job on channel: ${channel}`)
    }
}
