import {CommandInteraction} from "discord.js";
import {injectable} from "tsyringe";
import {Discord, Permission, Slash} from "discordx";
import ViewPermissionExecution from "./executions/ViewPermissionExecution.js";
import PermittedRoles from "../static/PermittedRoles.js";

@Discord()
@Permission(PermittedRoles.admins)
@injectable()
export default class ViewPermissionsCommand {


    @Slash('view-permissions')
    async viewPermissions(interaction: CommandInteraction) {
        const execution = new ViewPermissionExecution(interaction)
        await execution.execute()
    }


}
