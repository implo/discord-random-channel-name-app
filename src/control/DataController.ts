import {injectable} from "tsyringe";
import * as fs from "fs";

@injectable()
export default class DataController {

    private basePath: string = `${process.cwd()}/data/`

    private getAbsolutePath(path: string) {
        return `${this.basePath}${path}`
    }

    async readFile(path: string): Promise<string> {
        return await fs.promises.readFile(
            this.getAbsolutePath(path), 'utf-8'
        )
    }

    writeFile(path: string, content: string) {
        return fs.promises.writeFile(
            this.getAbsolutePath(path),
            content
        )
    }

    writeJson(path: string, object: any) {
        return this.writeFile(
            path,
            JSON.stringify(
                object,
                null, 2
            )
        )
    }

    async fileOrDirectoryExists(path: string | undefined = undefined) {
        try {
            const evaluatedPath = path ? `${this.basePath}/${path}` : this.basePath
            await fs.promises.access(evaluatedPath, fs.constants.F_OK)
            return true
        } catch (e) {
            console.log(e)
            return false
        }
    }

    removeItemFromJsonByCriteria(array: any[], criteria: (item: any) => void) {
        const itemsToDelete = array.filter(criteria)

        if (itemsToDelete.length !== 0) {
            itemsToDelete.forEach(item => {
                const index = array.indexOf(item)
                array.splice(index, 1)
            })
        } else {
            throw 'deleting failed'
        }

        return array
    }

    async createDirectory(path: string | undefined) {
        return fs.promises.mkdir(path ? path : this.basePath)
    }

    async createDirectoryIfMissing(path: string | undefined = undefined) {
        const directoryExists = await this.fileOrDirectoryExists(path)
        if (!directoryExists) {
            await this.createDirectory(path)
        }
    }
}
