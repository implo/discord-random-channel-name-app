import {singleton} from "tsyringe";
import CacheEntry from "./CacheEntry.js";
import Noun from "../typings/Noun.js";
import Adjective from "../typings/Adjective.js";
import JobDto from "../typings/JobDto.js"

@singleton()
export default class CacheController {

    public readonly nounCache = new CacheEntry<Noun[]>()
    public readonly adjectiveCache = new CacheEntry<Adjective[]>()
    public readonly jobCache = new CacheEntry<JobDto[]>()

    clearAll() {
        this.nounCache.clear()
        this.adjectiveCache.clear()
    }
}
