import {Guild, GuildChannel} from "discord.js";
import RandomGenerator from "./RandomGenerator.js";
import JobDto from "../typings/JobDto";
import {Client} from "discordx";
import RandomNameGenerator from "./RandomNameGenerator.js";
import PromiseUtil from "../static/PromiseUtil.js";

export default class RenameJob {

    public channelId: string
    private intervalObject?: NodeJS.Timer;
    private readonly minToMilli = 60000
    private readonly tryRenameTimeout = 1000

    constructor(
        private guild: Guild,
        private channel: GuildChannel,
        private generator: RandomGenerator<string>,
        private prefix: string = '',
        private intervalInMin: number = 30
    ) {
        this.channelId = this.channel.id
    }

    async start() {
        await this.tryRenamingWithTimeLimit()
        this.intervalObject = setInterval(async () => {
            console.log(`renaming channel ${this.channel}`)
            await this.tryRenamingWithTimeLimit()
        }, this.intervalInMilli)
    }

    get intervalInMilli() {
        return this.intervalInMin * this.minToMilli
    }

    private async tryRenamingWithTimeLimit() {
        try {
            await PromiseUtil.tryFor(this.tryRenameTimeout, this.rename())
        } catch (e: any) {
            const error = new Error('rename timeout. cannot rename because of discord restriction. skipping rename')
            console.log(error)
        }
    }

    private async rename() {
        await this.channel.setName(`${this.prefix.trim()} ${await this.generateName()}`)
    }

    private async generateName() {
        return this.generator
            .generate();
    }

    stop() {
        if (this.intervalObject) {
            clearInterval(this.intervalObject)
        } else {
            throw new Error('interval not active')
        }
    }

    get active() {
        return this.intervalObject != null
    }

    toDataObject(): JobDto {
        return {
            guildId: this.guild.id,
            channelId: this.channel.id,
            prefix: this.prefix,
            interval: this.intervalInMin
        }
    }

    static async fromDataObject(dto: JobDto, client: Client, generator: RandomNameGenerator) {
        const guild = await client.guilds.fetch(dto.guildId)
        const channel = await guild.channels.fetch(dto.channelId) as GuildChannel
        return new RenameJob(guild, channel, generator, dto.prefix, dto.interval)
    }
}
