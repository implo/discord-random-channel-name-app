export default interface RandomGenerator<T> {

    generate(): Promise<T>

}
