import {
    ColorResolvable,
    InteractionReplyOptions,
    MessageActionRow,
    MessageButton,
    MessageEmbed
} from "discord.js";

export default class PageWidget {

    private readonly pageChunks: string[][] = []

    constructor(
        private id: string,
        private title: string,
        private color: ColorResolvable,
        private data: string[],
        private ephemeral: boolean = true,
        private entriesPerList: number = 25,
        private page: number = 1
    ) {
        this.pageChunks = this.splitIntoPages()
    }

    get message(): InteractionReplyOptions & { fetchReply: true } {
        return {
            embeds: this.embedArray,
            components: this.actionRowArray,
            fetchReply: true,
            ephemeral:  this.ephemeral
        }
    }

    private get embed() {
        return new MessageEmbed()
            .setTitle(this.title)
            .setColor(this.color)
            .setDescription(this.description)
            .setFooter({text: this.footer})
    }

    private get embedArray() {
        return [this.embed]
    }

    private get description() {
        const pageChunk = this.pageChunks[this.page - 1]
        let content = ''

        pageChunk.forEach(entry => {
            content += `${entry} \n`
        })

        return content
    }

    private get footer() {
        return `Page ${this.page} / ${this.pageChunks.length}`
    }

    private get actionRow() {
        return new MessageActionRow()
            .addComponents(this.prevButton)
            .addComponents(this.nextButton)
    }

    private get actionRowArray() {
        return this.pageChunks.length > 1 ? [this.actionRow] : []
    }

    private get nextButton() {
        return PageWidget.createdButton(
            'next',
            '⏭️',
            `next-${this.id}`,
            !this.hasNextPage()
        )
    }

    private get prevButton() {
        return PageWidget.createdButton(
            'prev',
            '⏮️',
            `prev-${this.id}`,
            !this.hasPrevPage()
        )
    }

    private static createdButton(
        label: string,
        emoji: string,
        id: string,
        disabled: boolean = false
    ) {
        return new MessageButton()
            .setLabel(label)
            .setEmoji(emoji)
            .setStyle('PRIMARY')
            .setCustomId(id)
            .setDisabled(disabled)
    }

    nextPage() {
        this.page++
    }

    hasNextPage() {
        return this.page + 1 <= this.pageChunks.length
    }

    prevPage() {
        this.page--
    }

    hasPrevPage() {
        return this.page - 1 >= 1
    }

    private splitIntoPages() {
        const pageChunks = new Array(Math.ceil(this.data.length / this.entriesPerList))
            .fill("")
            .map(_ => this.data.splice(0, this.entriesPerList))

        if (pageChunks.length === 0) {
            pageChunks.push(["empty"])
        }

        return pageChunks
    }
}
