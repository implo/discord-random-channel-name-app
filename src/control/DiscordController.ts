import {injectable} from "tsyringe";
import {Client} from "discordx";

@injectable()
export default class DiscordController {


    constructor(private client: Client) {
    }

    async getGuildCommandsBy(guildId: string) {
        const guildCommandMap = await this.client.CommandByGuild()
        const guildCommands = guildCommandMap.get(guildId)
        return guildCommands ? guildCommands : []
    }

    async initializePermissions() {
        const guilds = await this.client.guilds.fetch()

        for (const guild of Array.from(guilds.values())) {
            const commands = await this.getGuildCommandsBy(guild.id)
            await this.client.initGuildApplicationPermissions(guild.id, commands, true)
        }
    }

}
