export default class CacheEntry<T> {

    public store?: T;

    get isFilled() {
        return this.store != null
    }

    set(store: T) {
        this.store = store
    }

    clear() {
        this.store = undefined
    }
}
