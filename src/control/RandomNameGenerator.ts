import RandomGenerator from "./RandomGenerator.js";
import {injectable} from "tsyringe";
import Adjective from "../typings/Adjective.js";
import Noun from "../typings/Noun.js";
import WordGenus from "../typings/WordGenus.js";
import AdjectiveDao from "../dao/AdjectiveDao.js";
import NounDao from "../dao/NounDao.js";

@injectable()
export default class RandomNameGenerator implements RandomGenerator<string> {

    constructor(
        private adjectiveDao: AdjectiveDao,
        private nounDao: NounDao
    ) {}

    get adjectives() {
        return this.adjectiveDao.getAll()
    }

    get nouns() {
        return this.nounDao.getAll()
    }

    async generate(): Promise<string> {
        const randomNoun = await this.generateNoun()
        const randomAdjective = await this.generateAdjective(randomNoun)
        return `${randomAdjective} ${randomNoun.text}`;
    }

    private async generateAdjective(noun: Noun) {
        const adjectives = await this.adjectives
        const randomIndex = RandomNameGenerator
            .getRandomIndexOf(adjectives.length)

        return RandomNameGenerator
            .evaluateGenus(await adjectives[randomIndex], noun)
    }

    private async generateNoun() {
        const nouns = await this.nouns
        const randomIndex = RandomNameGenerator
            .getRandomIndexOf(nouns.length)

        return nouns[randomIndex]
    }

    private static getRandomIndexOf(listLength: number) {
        return ~~(Math.random() * listLength)
    }


    private static evaluateGenus(adjective: Adjective, noun: Noun) {
        switch (noun.genus) {
            case WordGenus.FEMALE:
                return `${adjective.text}e`
            case WordGenus.MALE:
                return `${adjective.text}er`
            case WordGenus.NEUTER:
                return `${adjective.text}es`
            default:
                throw new Error('Genus is unknown')
        }
    }
}
