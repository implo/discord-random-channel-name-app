function loginDockerRegistry() {
  echo $ghToken | docker login ghcr.io -u quirinecker --password-stdin
}

function setupMultiArch() {
    docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
}

function buildAndPush() {
    setupMultiArch
    loginDockerRegistry
    docker buildx bake --set "*.platform=$platform" --push
}

function upload() {
    if [ -n "$sshConfig" ]; then
      scp .env.prod $sshConfig:$path
      scp docker-compose.yml $sshConfig:$path
    else
      scp .env.prod $user@$ip:$path
      scp docker-compose.yml $user@$ip:$path
    fi
}

function start() {
    if [ -n "$sshConfig" ]; then
        ssh $sshConfig "
        cd $path
        echo $ghToken | docker login ghcr.io -u quirinecker --password-stdin
        $dockerComposeLocation pull
        $dockerComposeLocation up -d
        "
    else
        ssh $user@$ip "
        cd $path
        echo $ghToken | docker login ghcr.io -u quirinecker --password-stdin
        $dockerComposeLocation pull
        $dockerComposeLocation up -d
        "
    fi
}
