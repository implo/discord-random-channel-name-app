FROM node:16-alpine as build

WORKDIR /app

COPY package*.json ./

RUN npm i

COPY . .

RUN npm run build \
    && rm -rf node_modules \
    && npm install --production \
    && cp -r node_modules dist \
    && cp -r package*.json dist \
    && cp firestore_auth.json dist

FROM node:16 as run

WORKDIR /app

COPY --from=build /app/dist /app

ENTRYPOINT ["node", "index.js"]
